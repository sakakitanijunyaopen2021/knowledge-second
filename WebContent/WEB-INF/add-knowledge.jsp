<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>チャンネル登録</title>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="navbar.jsp"%>

	<div class="col-12">
		<h4 class="title">ナレッジ追加</h4>
		<br>
		<c:forEach items="${ errorMessageList }" var="errorMessage">
			<div class="error-message">
				<c:out value="${ errorMessage }" />
			</div>
		</c:forEach>
		<br>
		<form action="add-knowledge" method="post">	
			<h4>チャンネル</h4>
			<div class="col-8">
					<select name="channelId" >
						<c:forEach items="${ channelList }" var="channel">
						<option value="${ channel.channelId }">${ channel.channelName }</option>
						<a href="list-knowledge?channelId=${ channel.channelId }">BACK</a> 
						</c:forEach>
					</select>
					<br>
					<br>
					<h5>ナレッジ</h5>
			<textarea name="knowledge" maxlength="66535" rows="8" cols="40"><c:out value="${ KnowledgeDto.knowledge }" /></textarea>
				<br>
				<div class="form-group row">
				<button type="submit" class="btn btn-primary">INSERT</button>
				</div>
			</div>
		</form>
	</div>

</body>
</html>