<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>チャンネル編集</title>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="col-12">
		<h4 class="title">チャンネル編集</h4>
		<c:forEach items="${ errorMessageList }" var="errorMessage">
			<div class="error-message">
				<c:out value="${ errorMessage }" />
			</div>
		</c:forEach>
		<br>
		<form action="edit-channel" method="post">
			<input type="hidden" name="channelId" value="${ channelDto.channelId }" />
			<input type="hidden" name="updateNumber" value="${ channelDto.updateNumber }" />
			<div class="form-group row">
				<label class="col-sm-2 head">チャンネル</label>
				<div class="col-sm-8">
					<input type="text" name="channelName" class="form-control" maxlength="30" value="${ channelDto.channelName }" />
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 head">概要</label>
				<div class="col-8">
					<textarea name="overview" style="resize: none;" class="form-control" maxlength="200" rows="4"><c:out value="${ channelDto.overview }" /></textarea>
				</div>
			</div>
			<div class="form-group row">
				<div class="mx-auto">
					<button type="reset" class="btn btn-secondary">リセット</button>
					<button type="submit" class="btn btn-primary">更新</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>