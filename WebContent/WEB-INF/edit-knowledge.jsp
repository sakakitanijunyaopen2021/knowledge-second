<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ナレッジ編集</title>
<script type="text/javascript">
<!--
	function deleteConfirm(knowledge) {
		return window.confirm("「" + knowledge + "」チャンネルを本当に削除しますか？");
	}
// -->
</script>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="col-12">
		<h4 class="title">ナレッジ編集</h4>
		<br>
		<c:forEach items="${ errorMessageList }" var="errorMessage">
			<div class="error-message">
				<c:out value="${ errorMessage }" />
			</div>
		</c:forEach>
		<form action="edit-knowledge" method="post">	
		<input type="hidden" name="knowledgeId" value="${ knowledgeListById.knowledgeId }" />
		<input type="hidden" name="updateNumber" value="${ knowledgeListById.updateNumber }" />
		<input type="hidden" name="userId" value="${ knowledgeListById.userId }" />
				<h4>チャンネル</h4>
		<div class="col-8">
					<select name="channelId" >
						<c:forEach items="${ channelList }" var="channel">
						<option value="${ channel.channelId }">${ channel.channelName }</option>
						<a href="list-knowledge?channelId=${ channel.channelId }">BACK</a> 
						</c:forEach>
					</select>
					<br>
					<br>
					<h5>ナレッジ</h5>
			<textarea name="knowledge" maxlength="66535" rows="8" cols="40"><c:out value="${ knowledgeListById.knowledge }" /></textarea>
				<div class="form-group row">
					<div class="mx-auto">
					<br>
					<button type="reset" class="btn btn-secondaqry">BACK</button>
					<button type="submit" class="btn btn-primary">UPDATE</button>
					<button formaction="delete-knowledge" type="submit" class="btn btn-danger" onclick="return deleteConfirm('${ knowledgeListById.knowledge }')">DELETE</button>
				
				</div>
			</div>
		</form>
	</div>


</body>
</html>