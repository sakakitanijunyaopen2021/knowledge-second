package jp.kronos;

import java.util.ArrayList;

import java.util.List;
import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.KnowledgeDto;
import jp.kronos.dto.ChannelDto;

public class FieldValidator {

	/**
	 * 入力チェ�?ク
	 * @param dto チャンネル�?報
	 * @return エラーメ�?セージ
	 */
	public static List<String> channelValidation(ChannelDto dto) {
		 List<String> errorMessageList = new ArrayList<>();
		
		if ("".equals(dto.getChannelName())) {
			errorMessageList.add("チャンネル名を入力をしてください");
		}
		
		if (dto.getChannelName().length() > 30) {
			errorMessageList.add("チャンネルは30字以内で入力してください");
		}
		
		if ("".equals(dto.getOverview())) {
			errorMessageList.add("概要を入力してください");
		}
		
		if (dto.getOverview().length() > 200) {
			errorMessageList.add("概要を200文字以内で入力してください");
		}
		
		return errorMessageList;
	}
	
	public static List<String> knowledgeValidation(KnowledgeDto dto) {
		
		List<String> errorMessageList = new ArrayList<>();
		if ("".equals(dto.getKnowledge())) {
			errorMessageList.add("ナレッジを入力してください");
		
		} 
		return errorMessageList;
	}
	
}
