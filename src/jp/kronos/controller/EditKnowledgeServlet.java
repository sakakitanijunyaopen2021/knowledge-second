package jp.kronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.naming.NamingException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.FieldValidator;
import jp.kronos.DataSourceManager;
import jp.kronos.dao.ChannelDao;
import jp.kronos.dto.ChannelDto;
import jp.kronos.dto.UserDto;

import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.KnowledgeDto;

/**
 * Servlet implementation class EditKnowledgeServlet
 */
@WebServlet("/edit-knowledge")
public class EditKnowledgeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 縲�1. 繧ｻ繝�繧ｷ繝ｧ繝ｳ縺ｮ繝ｦ繝ｼ繧ｶ繝ｼ諠�蝣ｱ遒ｺ隱�
		// 繧ｻ繝�繧ｷ繝ｧ繝ｳ髢句ｧ�
		HttpSession session = request.getSession(false);
		
		if (session == null || session.getAttribute("user") == null) {
			// 繝医ャ繝励�壹�ｼ繧ｸ縺ｫ遘ｻ蜍�
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		// 2. 繧ｻ繝�繧ｷ繝ｧ繝ｳ蜀�縺ｮ繧ｨ繝ｩ繝ｼ繝｡繝�繧ｻ繝ｼ繧ｸ縺ｮ莠､謠�
			request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));
			session.removeAttribute("errorMessageList");

		
		// 3. 繧ｻ繝�繧ｷ繝ｧ繝ｳ繧ｹ繧ｳ繝ｼ繝怜��縺ｮ繝翫Ξ繝�繧ｸ諠�蝣ｱ繧貞叙蠕励☆繧具ｼ医⊇縺ｨ繧薙←縺ｮ蝣ｴ蜷育ｩｺ�ｼ�
		KnowledgeDto knowledgedto = (KnowledgeDto)session.getAttribute("knowldgeDto");
		
		// 4. 繝翫Ξ繝�繧ｸ諠�蝣ｱ縺悟ｭ伜惠縺吶ｋ蝣ｴ蜷�
		// 5. 繝翫Ξ繝�繧ｸ諠�蝣ｱ縺ｯ蟄伜惠縺励↑縺�蝣ｴ蜷�
		if (session.getAttribute("knowledge") != null) {
			// 4 繧ｻ繝�繧ｷ繝ｧ繝ｳ縺ｮ繝翫Ξ繝�繧ｸ諠�蝣ｱ縺ｮ蜑企勁	
			session.removeAttribute("errorMessageList");
			
		} else {
			// 5 蟄伜惠縺励↑縺�縺ｨ縺阪���ｼｩ�ｼ､蜿門ｾ�&邏舌▼縺上リ繝ｬ繝�繧ｸ諠�蝣ｱ繧貞叙蠕� 
			KnowledgeDto knowledgeDto = new KnowledgeDto();
			
			// 繝翫Ξ繝�繧ｸID蜿門ｾ�&繧ｻ繝�繝�
			String knowledgeIdStr = request.getParameter("knowledgeId");
			int knowledgeId = Integer.parseInt(knowledgeIdStr);
			//knowledgeDto.setChannelId(knowledgeId);
			
			// 繝翫Ξ繝�繧ｸ諠�蝣ｱ縺ｮ蜿門ｾ�&繧ｻ繝�繝�
			try(Connection conn = DataSourceManager.getConnection()) {
				// 繝�繝ｼ繧ｿ繝吶�ｼ繧ｹ謗･邯壹�．ao繧､繝ｳ繧ｹ繧ｿ繝ｳ繧ｹ菴懈��
				KnowledgeDao knowledgeDao = new KnowledgeDao(conn);
				
				//繝翫Ξ繝�繧ｸ諠�蝣ｱ繧貞女縺大叙繧九◆繧√�ｮ驟榊�嶺ｽ懈��
				KnowledgeDto knowledgeListById = new KnowledgeDto();
				knowledgeListById = knowledgeDao.selectByKnowledgeld(knowledgeId);
				// 繧ｻ�ｿｽ?繧ｷ繝ｧ繝ｳ縺九ｉ繝ｭ繧ｰ繧､繝ｳ繝ｦ繝ｼ繧ｶ繝ｼ蜿門ｾ�&繝ｦ繝ｼ繧ｶ繝ｼ諠�蝣ｱ遒ｺ隱�
				UserDto user = (UserDto)session.getAttribute("user");
				//if	(user.getUserId() != knowledgeDto.getUserId() || !user.isAdministratorFlg()) { 
				//	response.sendRedirect("access-denied.jsp");
				//	return;
				//}
				if (!user.isAdministratorFlg() && knowledgeListById.getUserId() != user.getUserId()) {
					response.sendRedirect("access-denied.jsp");
					return;
				}
				
				// 6. 蜿門ｾ励＠縺溘リ繝ｬ繝�繧ｸ諠�蝣ｱ繧偵Μ繧ｯ繧ｨ繧ｹ繝医せ繧ｳ繝ｼ繝励↓菫晄戟縺吶ｋ
				request.setAttribute("knowledgeListById", knowledgeListById);
				// 7. 繝√Ε繝ｳ繝阪Ν諠�蝣ｱ繧貞叙蠕励＠縲√Μ繧ｯ繧ｨ繧ｹ繝医せ繧ｳ繝ｼ繝励↓菫晄戟縺吶ｋ
				// ? 縺ｪ繧薙〒縺��ｼ�
				ChannelDao channelDao = new ChannelDao(conn);
				List<ChannelDto> channelList = channelDao.selectAll();
				request.setAttribute("channelList", channelList);
				
				// 8. edit-knowledge縺ｫ霆｢騾�
				request.getRequestDispatcher("WEB-INF/edit-knowledge.jsp").forward(request, response);
				//response.sendRedirect("edit-knowledge?channelId" + "=" + channelId);

			} catch (SQLException | NamingException e) {
				
				e.printStackTrace();
				response.sendRedirect("system-error.jsp");
				
			}
			
		}
		  		
	}

	
	
	
	
	
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// 1.  繧ｻ繝�繧ｷ繝ｧ繝ｳ縺ｫ繝ｭ繧ｰ繧､繝ｳ諠�蝣ｱ縺悟ｭ伜惠縺吶ｋ縺狗｢ｺ隱搾ｼ�index.jsp縺ｫ霆｢騾�ｼ�
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("user") == null) {
			// 繝医ャ繝励�壹�ｼ繧ｸ縺ｫ遘ｻ蜍�
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		// 2. 縲�繝輔か繝ｼ繝�縺ｮ繝�繝ｼ繧ｿ繧貞叙蠕暦ｼ医メ繝｣繝ｳ繝阪ΝID,繝翫Ξ繝�繧ｸID,繝翫Ξ繝�繧ｸ,譖ｴ譁ｰ逡ｪ蜿ｷ�ｼ�
		KnowledgeDto knowledgeDto = new KnowledgeDto();
			//繝ｪ繧ｯ繧ｨ繧ｹ繝医�ｮ繝代Λ繝｡繝ｼ繧ｿ蜿門ｾ�
			request.setCharacterEncoding("UTF-8");
			knowledgeDto.setKnowledge(request.getParameter("knowledge"));
			knowledgeDto.setChannelId(Integer.parseInt(request.getParameter("channelId")));
			knowledgeDto.setUpdateNumber(Integer.parseInt(request.getParameter("updateNumber")));
			knowledgeDto.setKnowledgeId(Integer.parseInt(request.getParameter("knowledgeId")));
			knowledgeDto.setUserId(Integer.parseInt(request.getParameter("userId")));
			
		
		// 3. 縲�繝輔か繝ｼ繝�縺ｮ蜈･蜉帙メ繧ｧ繝�繧ｯ
			//繝翫Ξ繝�繧ｸ繧貞�･蜉帙＠縺ｦ縺上□縺輔＞�ｼ医お繝ｩ繝ｼ繝｡繝�繧ｻ繝ｼ繧ｸ�ｼ�
		List<String> errorMessageList = FieldValidator.knowledgeValidation(knowledgeDto);
		// 4. 縲�繧ｨ繝ｩ繝ｼ縺後≠繧句�ｴ蜷医Γ繝�繧ｻ繝ｼ繧ｸ繧偵そ繝�繧ｷ繝ｧ繝ｳ縺ｫ菫晄戟縺励�‘ditServlet縺ｫ霆｢騾�
		if (errorMessageList.size() != 0) {
			// 繝√Ε繝ｳ繝阪Ν逋ｻ骭ｲ逕ｻ髱｢縺ｫ驕ｷ遘ｻ縺吶ｋ
			session.setAttribute("errorMessageList", errorMessageList);
			response.sendRedirect("edit-knowledge");
			return;
		}	
		
		
		// 5. 縲�繝ｭ繧ｰ繧､繝ｳID縺ｫ讓ｩ髯舌′縺ｪ縺�蝣ｴ蜷�accessdeni縺ｫ繝ｪ繝�繧､繝ｬ繧ｯ繝�
		UserDto user = (UserDto)session.getAttribute("user");
		
		
		//if (!user.isAdministratorFlg() && knowledgeDto.getUserId() != user.getUserId()) {
		if	(user.getUserId() != knowledgeDto.getUserId() && !user.isAdministratorFlg()) { 
			response.sendRedirect("access-denied.jsp");
			return;
		} else {
			knowledgeDto.setUserId(user.getUserId());
		}
		
		// 繝�繝ｼ繧ｿ繝吶�ｼ繧ｹ縺ｫ謗･邯�
		try (Connection conn = DataSourceManager.getConnection()) {
		// 6. 縲�繝翫Ξ繝�繧ｸ諠�蝣ｱ繧呈峩譁ｰ縺吶ｋ
		
			KnowledgeDao knowledgeDao = new KnowledgeDao(conn);
			knowledgeDao.updateByChannelId(knowledgeDto);
		
		
		// 7. 縲�繝｡繝�繧ｻ繝ｼ繧ｸ繧呈峩譁ｰ縺励∪縺励◆(繝ｪ繧ｯ繧ｨ繧ｹ繝医せ繧ｳ繝ｼ繝励∈)
		request.setAttribute("message", "UPDATE CONPLETED");
			
		// 8. 縲�繝√Ε繝ｳ繝阪ΝID繧偵ヱ繝ｩ繝｡繝ｼ繧ｿ縺ｫ菫晄戟縺励�〕ist-knowledge縺ｫ繝ｪ繝�繧､繝ｬ繧ｯ繝�
			//String蝙九〒繧医＞縺ｮ縺九↑縺�
		response.sendRedirect("list-knowledge?channelId=" + Integer.parseInt(request.getParameter("channelId")));
		
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
			response.sendRedirect("system-error.jsp");	
		}
	
	}

}
