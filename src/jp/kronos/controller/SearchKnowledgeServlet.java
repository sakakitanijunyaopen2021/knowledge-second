package jp.kronos.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.DataSourceManager;
import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.KnowledgeDto;

/**
 * Servlet implementation class SearchKnowledgeServlet
 */
@WebServlet("/search-knowledge")
public class SearchKnowledgeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// 検索キーワードを取得する。
		String keyword = request.getParameter("keyword");
		
		HttpSession session = request.getSession(true);
		session.removeAttribute("keyword");
		
		try (Connection conn = DataSourceManager.getConnection()) {	
			
			KnowledgeDao dao = new KnowledgeDao(conn);
			if (keyword == "") {
				List<KnowledgeDto> knowledgeList = dao.selectAll();
		
			
				 //URIをリクエストに保持する
				request.setAttribute("uri", request.getRequestURI());
				
				//メッセージをリクエストに保持する
				request.setAttribute("message", session.getAttribute("message"));
				session.removeAttribute("message");
				
				// ナブバ�?�メ�?セージをリクエストに保持する
				request.setAttribute("navbarMessage", session.getAttribute("navbarMessage"));
				session.removeAttribute("navbarMessage");
					
				// チャンネル�?覧�?ータをリクエストに保持する
				request.setAttribute("knowledgeList", knowledgeList);
				
			} else {
				String[] keywordlist = keyword.split(" ");
				List<KnowledgeDto> knowledgeList = dao.selectByQueries(keywordlist);
				// チャンネル�?覧�?ータをリクエストに保持する
				request.setAttribute("knowledgeList", knowledgeList);
				request.setAttribute("keyword", keyword);
				session.setAttribute("keyword", keyword);
			}
			
			// リクエストスコープにセットするのここじゃないの？エラー出るから場所変えた
			//request.setAttribute("knowledgeList", knowledgeList);
		
			// チャンネル�?覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/list-knowledge.jsp").forward(request, response);
			
	} catch (SQLException | NamingException e) {				
			// 例外メッセージ表示
			e.printStackTrace();
			// シス�?�?エラー画面に遷移する
			response.sendRedirect("system-error.jsp");
			}
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
