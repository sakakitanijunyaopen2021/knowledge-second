package jp.kronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.DataSourceManager;
import jp.kronos.dao.ChannelDao;
import jp.kronos.dto.KnowledgeDto;
import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.UserDto;

/**
 * Servlet implementation class DeleteKnowledgeServlet
 */
@WebServlet(name = "delete-knowledge", urlPatterns = { "/delete-knowledge" })
public class DeleteKnowledgeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// トップページに移動する
			request.getRequestDispatcher("index.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// 1.セッションにログインしているユーザ情報の確認
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("user") == null) {
			// トップページに移動
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}

		// 2. フォームのデータを取得する（チャンネルID、ナレッジID、更新番号）
		// 2. 　フォームのデータを取得（チャンネルID,ナレッジID,ナレッジ,更新番号）
		KnowledgeDto knowledgeDto = new KnowledgeDto();
			//リクエストのパラメータ取得
		request.setCharacterEncoding("UTF-8");
		knowledgeDto.setChannelId(Integer.parseInt(request.getParameter("channelId")));
		knowledgeDto.setUpdateNumber(Integer.parseInt(request.getParameter("updateNumber")));
		//knowledgeDto.setKnowledgeId(Integer.parseInt(request.getParameter("knowledgeId")));
		int knowledgeId = Integer.parseInt(request.getParameter("knowledgeId"));
		
		// 3.ログインユーザーが管理権限を持たない場合
		UserDto user = (UserDto)session.getAttribute("user");
		if (!user.isAdministratorFlg() && knowledgeDto.getUserId() != user.getUserId()) {
			//response.sendRedirect("access-denied.jsp");
			//return;
		}
		
		
		// 4.ナレッジ情報を削除する
			//データベース接続
		try (Connection conn = DataSourceManager.getConnection()) {
			KnowledgeDao knowledgeDao = new KnowledgeDao(conn);
			knowledgeDao.deleteByKnowledgeId(knowledgeId);
			
		// 5.メッセージをセッションスコープに保持する 
			request.setAttribute("message", "DELETE CONPLETED");
		// 6. チャンネルIDをパラメータに付与し、ListKnowledgeServletにリダイレクト
			response.sendRedirect("list-knowledge?channelId=" + Integer.parseInt(request.getParameter("channelId")));	
			
		} catch (SQLException | NamingException e) {	
			
		}
	}

}
