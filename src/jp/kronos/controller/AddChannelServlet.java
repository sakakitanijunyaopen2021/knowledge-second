package jp.kronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.FieldValidator;
import jp.kronos.DataSourceManager;
import jp.kronos.dao.ChannelDao;
import jp.kronos.dto.ChannelDto;
import jp.kronos.dto.UserDto;

@WebServlet("/add-channel")
public class AddChannelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// セ�?ションを取得す�?
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("user") == null) {
			// トップ�?��?�ジ(チャンネル�?覧)に遷移する
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		// エラーメ�?セージをリクエストに保持する
		request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));
		session.removeAttribute("errorMessageList");
		
		// チャンネル登録画面に遷移する
		request.getRequestDispatcher("WEB-INF/add-channel.jsp").forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// セ�?ションを閉じる
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("user") == null) {
			// トップ�?��?�ジ(チャンネル�?覧)に遷移する
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		// セ�?ションからログインユーザ�?報を取得す�?
		UserDto user = (UserDto)session.getAttribute("user");
		
		// フォー�?の�?ータを取得す�?
		request.setCharacterEncoding("UTF-8");
		ChannelDto channelDto = new ChannelDto();
		channelDto.setChannelName(request.getParameter("channelName"));
		channelDto.setOverview(request.getParameter("overview"));
		channelDto.setUserId(user.getUserId());
		
		// 入力チェ�?ク
		List<String> errorMessageList = FieldValidator.channelValidation(channelDto);
		if (errorMessageList.size() != 0) {
			// チャンネル登録画面に遷移する
			session.setAttribute("errorMessageList", errorMessageList);
			response.sendRedirect("add-channel");
			return;
		}
		
		// コネクションを取得す�?
		try (Connection conn = DataSourceManager.getConnection()) {
			// チャンネルを追�?する
			ChannelDao channelDao = new ChannelDao(conn);
			channelDao.insert(channelDto);

			// チャンネル名をリクエストスコープに保持する
			session.setAttribute("message", channelDto.getChannelName() + "を登録しました");
			
			// チャンネル�?覧画面に遷移する
			response.sendRedirect("list-channel");
			
		} catch (SQLException | NamingException e) {
						
			// チャンネル名が重�?して�?る�?��?
			if (e.getMessage().contains("Duplicate entry")) {
				
				// エラーメ�?セージをセ�?ションスコープに保持する
				errorMessageList.add("チャンネル[" + channelDto.getChannelName() + "]はすでに存在します");
				session.setAttribute("errorMessageList", errorMessageList);
				
				// フォームデータををセ�?ションスコープに保持する
				session.setAttribute("channelDto", channelDto);

				// チャンネル登録画面に遷移する
				response.sendRedirect("add-channel");
				
			} else {
				// 例外メ�?セージを�?�力表示
				e.printStackTrace();
				
				// シス�?�?エラー画面に遷移する
				response.sendRedirect("system-error.jsp");
			}
		}
	}
}
