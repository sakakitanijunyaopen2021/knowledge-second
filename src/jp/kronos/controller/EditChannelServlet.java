package jp.kronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.DataSourceManager;
import jp.kronos.FieldValidator;
import jp.kronos.dao.ChannelDao;
import jp.kronos.dto.ChannelDto;
import jp.kronos.dto.UserDto;

@WebServlet("/edit-channel")
public class EditChannelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// セ�?ションを取得す�?
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("user") == null) {
			// トップ�?��?�ジ(チャンネル�?覧)に遷移する
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}

		// ログインユーザ�?報を取得す�?
		UserDto user = (UserDto) session.getAttribute("user");

		// エラーメ�?セージをリクエストに保持する
		request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));
		session.removeAttribute("errorMessageList");

		ChannelDto channelDto = (ChannelDto) session.getAttribute("channelDto");

		// セ�?ションスコープにチャンネル�?報がある�?��?
		if (channelDto != null) {
			// セ�?ションスコープからチャンネル�?報を削除する
			session.removeAttribute("channelDto");
		} else {
			// フォームデータのID取得
			request.setCharacterEncoding("UTF-8");
			int channelId = Integer.parseInt(request.getParameter("channelId"));
			//int knowledgeId = Integer.parseInt(request.getParameter("knowledgeId"));
			
			// コネクションを取得す�?
			try (Connection conn = DataSourceManager.getConnection()) {

				// ナレ�?ジ�?報を取得す�?
				ChannelDao channelDao = new ChannelDao(conn);

				// チャンネル�?報を取得す�?
				channelDto = channelDao.selectByChannelId(channelId);

				// ログインユーザに管�?権限がな�?、かつナレ�?ジのユーザIDと�?致しな�?場�?
				if (!user.isAdministratorFlg() && channelDto.getUserId() != user.getUserId()) {
					response.sendRedirect("access-denied.jsp");
					return;
				}

			} catch (SQLException | NamingException e) {
				
				// 例外メ�?セージを�?�力表示
				e.printStackTrace();

				// シス�?�?エラー画面に遷移する
				response.sendRedirect("system-error.jsp");
				return;
			}
		}
		// TODO ログインユーザ更新時に、管�?�?ユーザが�?�に更新してしまった�?�合�?�dtoが取得できな�?可能性の�?慮
		request.setAttribute("channelDto", channelDto);

		// チャンネル編�?画面に遷移する
		request.getRequestDispatcher("WEB-INF/edit-channel.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// セ�?ションを取得す�?
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("user") == null) {
			// トップ�?��?�ジ(チャンネル�?覧)に遷移する
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}

		// ログインユーザ�?報を取得す�?
		UserDto user = (UserDto) session.getAttribute("user");

		// フォー�?の�?ータを取得す�?
		request.setCharacterEncoding("UTF-8");
		ChannelDto channelDto = new ChannelDto();
		channelDto.setChannelId(Integer.parseInt(request.getParameter("channelId")));
		channelDto.setChannelName(request.getParameter("channelName"));
		channelDto.setOverview(request.getParameter("overview"));
		channelDto.setUpdateNumber(Integer.parseInt(request.getParameter("updateNumber")));

		// 入力チェ�?ク
		List<String> errorMessageList = FieldValidator.channelValidation(channelDto);
		if (errorMessageList.size() != 0) {
			// チャンネル編�?画面に遷移する
			session.setAttribute("errorMessageList", errorMessageList);
			session.setAttribute("channelDto", channelDto);
			response.sendRedirect("edit-channel?channelId=" + request.getParameter("channelId"));
			return;
		}

		// コネクションを取得す�?
		try (Connection conn = DataSourceManager.getConnection()) {

			ChannelDao channelDao = new ChannelDao(conn);

			// ログインユーザに管�?権限がな�?、かつナレ�?ジのユーザIDと�?致しな�?場�?
			if (!user.isAdministratorFlg() && channelDao.selectByChannelId(channelDto.getChannelId()).getUserId() != user.getUserId()) {
				response.sendRedirect("access-denied.jsp");
				return;
			}

			// チャンネルを更新する
			channelDao.update(channelDto);

			// チャンネル名をリクエストスコープに保持する
			session.setAttribute("message", channelDto.getChannelName() + "を更新しました");

			// チャンネル�?覧画面に遷移する
			response.sendRedirect("list-channel");

		} catch (SQLException | NamingException e) {

			// チャンネル名が重�?して�?る�?��?
			if (e.getMessage().contains("Duplicate entry")) {

				// エラーメ�?セージをリクエストスコープに保持する
				errorMessageList.add("チャンネル�?" + channelDto.getChannelName() + "」�?�既に存在しま�?");
				session.setAttribute("errorMessageList", errorMessageList);

				// フォー�?の�?ータをリクエストスコープに保持する
				session.setAttribute("channelDto", channelDto);

				// チャンネル編�?画面に遷移する
				response.sendRedirect("edit-channel?channelId=" + request.getParameter("channelId"));
			} else {
				// 例外メ�?セージを�?�力表示
				e.printStackTrace();
				
				// シス�?�?エラー画面に遷移する
				response.sendRedirect("system-error.jsp");
			}
		}
	}

}
