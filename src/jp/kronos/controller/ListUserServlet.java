package jp.kronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.DataSourceManager;
import jp.kronos.dto.UserDto;
import jp.kronos.DataSourceManager;
import jp.kronos.dao.UserDao;

/**
 * Servlet implementation class ListUserServlet
 */
@WebServlet(name = "list-user", urlPatterns = { "/list-user" })
public class ListUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//1. セッションにログインしているユーザ情報が入っていない、または権限がない場合index.jspに遷移する
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("user") == null) {
			// トップ�?��?�ジ(チャンネル�?覧)に遷移する
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		//2. ユーザ情報リストを全件取得し、リクエストスコープに保持する
		try (Connection conn = DataSourceManager.getConnection()) {
			UserDto userDto = new UserDto();
			UserDao userDao = new UserDao(conn);
			List<UserDto> userList = new ArrayList<>();
			userList = userDao.selectAll();
			
			//3. セッションスコープ内にエラーメッセージをリクエストスコープに保持し、セッションスコープのエラーメッセージを削除
			request.setAttribute("errorMessage", session.getAttribute("errorMessage"));
			session.removeAttribute("errormessage");
			
			//4. セッションスコープ内にメッセージをリクエストスコープに保持し、セッションスコープのメッセージを削除
			request.setAttribute("message", session.getAttribute("message"));
			session.removeAttribute("message");
			
			//5. list-user.jsp
			request.getRequestDispatcher("list-user.jsp").forward(request, response);
			//jspの位置を修正すること
			
		} catch (SQLException | NamingException e) {
			//エラーを出力表示
			e.printStackTrace();
			// システムエラー画面に遷移する
            response.sendRedirect("system-error.jsp");
		}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			doGet(request, response);
	}

}
