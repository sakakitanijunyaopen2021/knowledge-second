package jp.kronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;


import jp.kronos.dto.UserDto;

/**
 * ナレ�?ジ�?ーブルのDataAccessObject
 * @author Mr.X
 */
public class UserDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定す�?
	 * @param conn コネクション
	 */
    public UserDao(Connection conn) {
        this.conn = conn;
    }
    
    public List<UserDto> selectAll() throws SQLException {
    	
    	//SQL文作成
    	StringBuffer sb = new StringBuffer();
    	sb.append("SELECT");
    	sb.append("        user_id");
    	sb.append("        ,email");
    	sb.append("        ,password");
    	sb.append("        ,first_name");
    	sb.append("        ,last_name");
    	sb.append("        ,administrator_flg");
    	sb.append("        ,update_user_id");
    	sb.append("        ,update_number");
    	sb.append("        ,del_flg");
    	sb.append("    FROM");
    	sb.append("        USER;");
    	
    	//データベース接続
    	try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
    		
    		//SQL文実行
    		ResultSet rs = ps.executeQuery();
    		List<UserDto> userList = new ArrayList<>();
    		
    		// 結果をDTOに詰める
    			//del_flg はBoolean型
                while (rs.next()) {
                UserDto user = new UserDto();
                user.setUserId(rs.getInt("USER_ID"));
                user.setEmail(rs.getString("EMAIL"));
                user.setFirstName(rs.getString("FIRST_NAME"));
                user.setLastName(rs.getString("LAST_NAME"));
                user.setAdministratorFlg(rs.getBoolean("ADMINISTRATOR_FLG"));
                user.setUpdateUserId(rs.getInt("UPDATE_USER_ID"));
                user.setUpdateAt(rs.getTimestamp("UPDATE_AT"));
                user.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
                user.setDelFlg(rs.getBoolean("DEL_FLG"));
                userList.add(user);
            }
            return userList;
        }
    	
    }
    /**
     * ユーザ�?報を取得す�?
     * @param id ID
     * @param password パスワー�?
     * @return ユーザ�?報
     * @throws SQLException SQL例�?
     */
    public UserDto findByIdAndPassword(String id, String password) throws SQLException {

    	// SQL�?を作�?�す�?
    	StringBuffer sb = new StringBuffer();
    	sb.append(" select");
    	sb.append("        USER_ID");
    	sb.append("       ,EMAIL");
    	sb.append("       ,FIRST_NAME");
    	sb.append("       ,LAST_NAME");
    	sb.append("       ,ADMINISTRATOR_FLG");
    	sb.append("       ,UPDATE_USER_ID");
    	sb.append("       ,UPDATE_AT");
    	sb.append("       ,UPDATE_NUMBER");
    	sb.append("       ,DEL_FLG");
    	sb.append("   from USER");
    	sb.append("  where EMAIL = ?");
    	sb.append("    and PASSWORD = sha2(?, 256)");
    	sb.append("    and DEL_FLG = 0");

    	// ス�?ートメントオブジェクトを作�?�す�?
        try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
        	// プレースホル�?ーに値をセ�?トす�?
            ps.setString(1, id);
            ps.setString(2, password);
            
            // SQLを実行す�?
            ResultSet rs = ps.executeQuery();
            
            // 結果をDTOに詰める
            if (rs.next()) {
                UserDto user = new UserDto();
                user.setUserId(rs.getInt("USER_ID"));
                user.setEmail(rs.getString("EMAIL"));
                user.setFirstName(rs.getString("FIRST_NAME"));
                user.setLastName(rs.getString("LAST_NAME"));
                user.setAdministratorFlg(rs.getBoolean("ADMINISTRATOR_FLG"));
                user.setUpdateUserId(rs.getInt("UPDATE_USER_ID"));
                user.setUpdateAt(rs.getTimestamp("UPDATE_AT"));
                user.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
                user.setDelFlg(rs.getBoolean("DEL_FLG"));
                return user;
            }
            // 該当するデータがな�?場合�?�nullを返却する
        	return null;
        }
    }
}
