package jp.kronos.dao;

import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.kronos.DataSourceManager;
import jp.kronos.dao.ChannelDao;
import jp.kronos.dto.ChannelDto;
import jp.kronos.dto.KnowledgeDto;

/**
 * ナレ�?ジ�?ーブルのDataAccessObject
 * @author Mr.X
 */
public class KnowledgeDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定す�?
	 * @param conn コネクション
	 */
	public KnowledgeDao(Connection conn) {
		this.conn = conn;
	}
	
	
	public List<KnowledgeDto> selectAll() throws SQLException {
		// list作成	
		List<KnowledgeDto> list = new ArrayList<>();
		
		// SQL文形成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        knowledge.update_at");
		sb.append("        ,knowledge.knowledge_id");
		sb.append("        ,channel.channel_name");
		sb.append("        ,knowledge");
		sb.append("        ,knowledge.update_number");
		sb.append("        ,knowledge.user_id");
		sb.append("    FROM");
		sb.append("        knowledge");
		sb.append("           Inner JOIN channel");
		sb.append("                ON channel.channel_id = knowledge.channel_id");
		sb.append("    ORDER BY");
		sb.append("        knowledge.channel_id;");


    	// ス�?ートメントオブジェクトを作�?�す�?
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			
            // SQLを実行する
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				KnowledgeDto dto = new KnowledgeDto();
				// ナレッジ情報、チャンネルIDをセットする
				dto.setKnowledge(rs.getString("knowledge"));
				dto.setKnowledgeId(rs.getInt("knowledge.knowledge_id"));
				dto.setChannelName(rs.getString("channel.channel_name"));
				dto.setUserId(rs.getInt("knowledge.user_id"));
				dto.setUpdateNumber(rs.getInt("knowledge.update_number"));
				dto.setUpdateAt(rs.getTimestamp("knowledge.update_at"));
				list.add(dto);
			}
			return list;
		}
	
	}
	
	public List<KnowledgeDto> selectByQueries(String[] keywordList) throws SQLException {
			
	    	// SQL文の作成
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        knowledge.update_at");
			sb.append("        ,knowledge.knowledge_id");
			sb.append("        ,channel.channel_name");
			sb.append("        ,knowledge");
			sb.append("        ,knowledge.update_number");
			sb.append("        ,knowledge.user_id");
			sb.append("    FROM");
			sb.append("        knowledge");
			sb.append("            JOIN channel");
			sb.append("                ON channel.channel_id = knowledge.channel_id");
			sb.append("    WHERE");
			sb.append("        knowledge LIKE ?");
			for(int i = 1; i < keywordList.length; i++) {
				sb.append("    AND");
				sb.append("        knowledge LIKE ?");
			}
			sb.append("    ORDER BY");
			sb.append("        knowledge_id;");



			List<KnowledgeDto> list = new ArrayList<>();
	    	// ス�?ートメントオブジェクトを作�?�す�?
			try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
	        	// プレースホル�?ーに値をセ�?トす�?
				
				for(int i = 0; i < keywordList.length; i++) {
					ps.setString(i + 1, "%" + keywordList[i] + "%");
				}
						

	            // SQLを実行す�?
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					KnowledgeDto dto = new KnowledgeDto();
					// ナレッジ情報、チャンネルIDをセットする
					dto.setKnowledge(rs.getString("knowledge"));
					dto.setChannelName(rs.getString("channel_name"));
					dto.setUpdateAt(rs.getTimestamp("update_at"));
					dto.setUpdateNumber(rs.getInt("update_number"));
					dto.setUserId(rs.getInt("user_id"));
					dto.setKnowledgeId(rs.getInt("knowledge_id"));
					list.add(dto);
				}
				return list;
			}
		
	
		
	}
	
	/**
	 * チャンネルIDに該当するナレッジ情報を取得する
	 * @param channelId
	 * 若干誤字してるから余裕あったらメソッドを使用する箇所で変更すること
	 */
	public List<KnowledgeDto> selectByChannelld(int channelId) throws SQLException {
		
    	// SQL文の作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        knowledge");
		sb.append("        ,channel.channel_name");
		sb.append("        ,knowledge.update_at");
		sb.append("        ,channel.channel_id");
		sb.append("        ,knowledge.user_id");
		sb.append("        ,knowledge.knowledge_id");
		sb.append("        ,knowledge.update_number");
		sb.append("    FROM");
		sb.append("        knowledge");
		sb.append("            JOIN channel");
		sb.append("                ON channel.channel_id = knowledge.channel_id");
		sb.append("     WHERE");
		sb.append("        channel.channel_id = ?");
		sb.append("    ORDER BY");
		sb.append("        knowledge_id;");


		List<KnowledgeDto> list = new ArrayList<>();
    	// ス�?ートメントオブジェクトを作�?�す�?
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
        	// プレースホル�?ーに値をセ�?トす�?
			ps.setInt(1, channelId);

            // SQLを実行す�?
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				KnowledgeDto dto = new KnowledgeDto();
				// ナレッジ情報、チャンネルIDをセットする
				dto.setKnowledge(rs.getString("knowledge"));
				dto.setChannelName(rs.getString("channel_name"));
				dto.setUpdateAt(rs.getTimestamp("update_at"));
				dto.setUserId(rs.getInt("user_id"));
				dto.setKnowledgeId(rs.getInt("knowledge_id"));
				dto.setUpdateNumber(rs.getInt("update_number"));
				list.add(dto);
			}
			return list;
		}
	}
	
	public KnowledgeDto selectByKnowledgeld(int knowledgeId) throws SQLException {
		
    	// SQL文の作成
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        knowledge");
		sb.append("        ,channel.channel_name");
		sb.append("        ,knowledge.update_at");
		sb.append("        ,channel.channel_id");
		sb.append("        ,knowledge.user_id");
		sb.append("        ,knowledge.knowledge_id");
		sb.append("        ,knowledge.update_number");
		sb.append("    FROM");
		sb.append("        knowledge");
		sb.append("            JOIN channel");
		sb.append("                ON channel.channel_id = knowledge.channel_id");
		sb.append("     WHERE");
		sb.append("        knowledge.knowledge_id = ?");
		//sb.append("    ORDER BY");
		//sb.append("        knowledge_id;");

		KnowledgeDto dto = null;
		
    	// ス�?ートメントオブジェクトを作�?�す�?
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
        	// プレースホル�?ーに値をセ�?トす�?
			ps.setInt(1, knowledgeId);

            // SQLを実行す�?
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				dto = new KnowledgeDto();
				// ナレッジ情報、チャンネルIDをセットする
				dto.setKnowledge(rs.getString("knowledge"));
				dto.setChannelName(rs.getString("channel_name"));
				dto.setUpdateAt(rs.getTimestamp("update_at"));
				dto.setUserId(rs.getInt("user_id"));
				dto.setKnowledgeId(rs.getInt("knowledge_id"));
				dto.setUpdateNumber(rs.getInt("update_number"));;
			}
			return dto;
		}
	}
	
	/**
	 * knowledgeのadd処理
	 * @param channelId
	 * @throws SQLException
	 */
	public void insert(KnowledgeDto dto) throws SQLException {
		
    	// SQL�?を作�?�す�?
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        knowledge(");
		sb.append("            knowledge");
		sb.append("            ,user_id");
		sb.append("            ,channel_id");
		sb.append("            ,update_number");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,0");
		sb.append("        );");

		//KnowledgeDtoインスタンス形成
		//KnowledgeDto dto = new KnowledgeDto();
		
    	// ス�?ートメントオブジェクトを作�?�す�?
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
        	// プレースホル�?ーに値をセ�?トす�?
			ps.setString(1, dto.getKnowledge());
			ps.setInt(2, dto.getUserId());
			ps.setInt(3, dto.getChannelId());
			
            // SQLを実行する
			ps.executeUpdate();
		}
	}
	
	public void updateByChannelId(KnowledgeDto dto) throws SQLException {
		
    	// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        knowledge");
		sb.append("    SET");
		sb.append("        knowledge = ?");
		sb.append("        ,update_number = update_number + 1");
		sb.append("        ,user_id = ?");
		sb.append("    WHERE");
		sb.append("        knowledge_id = ?;");

    	// ス�?ートメントオブジェクトを作�?�す�?
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
        	// プレースホル�?ーに値をセ�?トす�?
			ps.setString(1, dto.getKnowledge());
			ps.setInt(2, dto.getUserId());
			ps.setInt(3, dto.getKnowledgeId());
			//ps.setInt(5, dto.getChannelId());
            // SQLを実行す�?
			ps.executeUpdate();
		}
	}
	
	/**
	 * ナレ�?ジ�?報を削除する
	 * @param channelId チャンネルID
	 * @throws SQLException SQL例�?
	 */
	public void deleteByChannelId(int channelId) throws SQLException {
		
    	// SQL�?を作�?�す�?
		StringBuffer sb = new StringBuffer();
		sb.append(" delete from KNOWLEDGE");
		sb.append("       where CHANNEL_ID = ?");

    	// ス�?ートメントオブジェクトを作�?�す�?
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
        	// プレースホル�?ーに値をセ�?トす�?
			ps.setInt(1, channelId);

            // SQLを実行す�?
			ps.executeUpdate();
		}
	}
	

	/**
	 * ナレ�?ジ�?報を削除する
	 * @param channelId チャンネルID
	 * @throws SQLException SQL例�?
	 */
	public void deleteByKnowledgeId(int knowledgeId) throws SQLException {
		
    	// SQL�?を作�?�す�?
		StringBuffer sb = new StringBuffer();
		sb.append(" delete from KNOWLEDGE");
		sb.append("       where Knowledge_Id = ?");

    	// ス�?ートメントオブジェクトを作�?�す�?
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
        	// プレースホル�?ーに値をセ�?トす�?
			ps.setInt(1, knowledgeId);

            // SQLを実行す�?
			ps.executeUpdate();
		}
	}
	
	
}
