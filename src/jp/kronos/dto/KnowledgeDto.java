package jp.kronos.dto;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class KnowledgeDto {

	/* ãã¬ã?ã¸ID */
	private int knowledgeId;

	/* ãã¬ã?ã¸ */
	private String knowledge;

	/* ãã£ã³ãã«ID */
	private int channelId;

	/* ãã£ã³ãã«å? */
	private String channelName;
	
	/* ã¦ã¼ã¶ID */
	private int userId;

	/* æ´æ°æå» */
	private Timestamp updateAt;

	/* æ´æ°çªå· */
	private int updateNumber;

	/**
	 * ãã¬ã?ã¸IDãåå¾ãã?
	 * @return ãã¬ã?ã¸ID
	 */
	public int getKnowledgeId() {
		return knowledgeId;
	}

	/**
	 * ãã¬ã?ã¸IDãè¨­å®ãã?
	 * @param knowledgeId ãã¬ã?ã¸ID
	 */
	public void setKnowledgeId(int knowledgeId) {
		this.knowledgeId = knowledgeId;
	}

	/**
	 * ãã¬ã?ã¸ãåå¾ãã?
	 * @return ãã¬ã?ã¸
	 */
	public String getKnowledge() {
		return knowledge;
	}

	/**
	 * ãã¬ã?ã¸ãè¨­å®ãã?
	 * @param knowledge ãã¬ã?ã¸
	 */
	public void setKnowledge(String knowledge) {
		this.knowledge = knowledge;
	}

	/**
	 * ãã£ã³ãã«IDãåå¾ãã?
	 * @return ãã£ã³ãã«ID
	 */
	public int getChannelId() {
		return channelId;
	}

	/**
	 * ãã£ã³ãã«IDãè¨­å®ãã?
	 * @param channelId ãã£ã³ãã«ID
	 */
	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	/**
	 * ãã£ã³ãã«åãåå¾ãã?
	 * @return ãã£ã³ãã«å?
	 */
	public String getChannelName() {
		return channelName;
	}

	/**
	 * ãã£ã³ãã«åãè¨­å®ãã?
	 * @param channelName ãã£ã³ãã«å?
	 */
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	/**
	 * ã¦ã¼ã¶IDãåå¾ãã?
	 * @return ã¦ã¼ã¶ID
	 */
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * æ´æ°æå»ãåå¾ãã?
	 * @return æ´æ°æå»
	 */
	public String getUpdateAt() {
		return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(updateAt);
//		return updateAt;
	}

	/**
	 * æ´æ°æå»ãè¨­å®ãã?
	 * @param updateAt æ´æ°æå»
	 */
	public void setUpdateAt(Timestamp updateAt) {
		this.updateAt = updateAt;
	}

	/**
	 * æ´æ°ã¦ã¼ã¶IDãåå¾ãã?
	 * @return æ´æ°ã¦ã¼ã¶ID
	 */
	public int getUpdateNumber() {
		return updateNumber;
	}

	/**
	 * æ´æ°ã¦ã¼ã¶IDãè¨­å®ãã?
	 * @param updateNumber æ´æ°ã¦ã¼ã¶ID
	 */
	public void setUpdateNumber(int updateNumber) {
		this.updateNumber = updateNumber;
	}

}