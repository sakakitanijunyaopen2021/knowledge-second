package jp.kronos.dto;

import java.sql.Timestamp;

/**
 * チャンネル�?報
 * @author Mr.X
 */
public class ChannelDto {

	/* チャンネルID */
	private int channelId;
	
	/* チャンネル�? */
	private String channelName;
	
	/* 概�? */
	private String overview;
	
	/* ユーザID */
	private int userId;
	
	/* 更新時刻 */
	private Timestamp updateAt;

	/* 更新番号 */
	private int updateNumber;

	/**
	 * チャンネルIDを取得す�?
	 * @return the channelId
	 */
	public int getChannelId() {
		return channelId;
	}

	/**
	 * チャンネルIDを設定す�?
	 * @param channelId チャンネルID
	 */
	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	/**
	 * チャンネル名を取得す�?
	 * @return チャンネル�?
	 */
	public String getChannelName() {
		return channelName;
	}

	/**
	 * チャンネル名を設定す�?
	 * @param channelName チャンネル�?
	 */
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	/**
	 * 概要を取得す�?
	 * @return 概�?
	 */
	public String getOverview() {
		return overview;
	}

	/**
	 * 概要を設定す�?
	 * @param overview 概�?
	 */
	public void setOverview(String overview) {
		this.overview = overview;
	}

	/**
	 * ユーザIDを取得す�?
	 * @return ユーザID
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * ユーザIDを設定す�?
	 * @param userId ユーザID
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * 更新時刻を取得す�?
	 * @return 更新時刻
	 */
	public Timestamp getUpdateAt() {
		return updateAt;
	}

	/**
	 * 更新時刻を設定す�?
	 * @param updateAt 更新時刻
	 */
	public void setUpdateAt(Timestamp updateAt) {
		this.updateAt = updateAt;
	}

	/**
	 * 更新番号を取得す�?
	 * @return 更新番号
	 */
	public int getUpdateNumber() {
		return updateNumber;
	}

	/**
	 * 更新番号を設定す�?
	 * @param updateNumber 更新番号
	 */
	public void setUpdateNumber(int updateNumber) {
		this.updateNumber = updateNumber;
	}
	
}
